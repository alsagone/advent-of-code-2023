
from get_input import get_input
import re 

pattern_game_id = re.compile(r"Game (\d+)")
pattern_game_sets = re.compile(r"(\d+) (blue|red|green)")
MAX_RED = 12 
MAX_GREEN = 13
MAX_BLUE = 14

def get_game_id(game_str: str) -> int:
    global pattern_game_id
    matches = re.search(pattern_game_id, game_str)
    
    if not(matches):
        raise f'No Game ID found for "{game_str}"'
    
    return int(matches.group(1))


def is_game_valid(game_str: str) -> bool:
    is_valid = True
    sets_start = game_str.index(':') + 1
    sets_str = game_str[sets_start:]
    
    for game_set in sets_str.split(";"):
        matches = re.findall(pattern_game_sets, game_set)
        
        for m in matches:
            nb_cubes, color = int(m[0]), m[1]
            
            if color == "red":
                is_valid = (nb_cubes <= MAX_RED) 
                
            elif color == "green":
                is_valid = (nb_cubes <= MAX_GREEN)
                
            elif color == "blue":
                is_valid = (nb_cubes <= MAX_BLUE)
                
            if not(is_valid):
                return False
             
    return True

def part_one(input_arr: list[str]) -> int:
    total_sum = 0
    
    for game_str in input_arr:
        if (is_game_valid(game_str)):
            total_sum += get_game_id(game_str)
    
    return total_sum 

# Part two 
def get_game_power(game_str: str) -> int:
    max_cubes = {
        "red": 0,
        "green": 0,
        "blue": 0
    }
    
    sets_start = game_str.index(':') + 1
    sets_str = game_str[sets_start:]
    
    for game_set in sets_str.split(";"):
        matches = re.findall(pattern_game_sets, game_set)
        
        for m in matches:
            nb_cubes, color = int(m[0]), m[1]
            
            # Trouver le max nb_cubes valide de chaque couleur
            if color == "red":
                max_cubes["red"] = max(nb_cubes, max_cubes["red"])
                
            elif color == "green":
                max_cubes["green"] = max(nb_cubes, max_cubes["green"])
                
            elif color == "blue":
                max_cubes["blue"] = max(nb_cubes, max_cubes["blue"])
            
    return max_cubes["red"] * max_cubes["green"] * max_cubes["blue"]
    
def part_two(input_arr: list[str]) -> int:
    total_sum = 0
    
    for game_str in input_arr:
        total_sum += get_game_power(game_str)
    
    return total_sum

if __name__ == "__main__":
    input_arr = get_input(2)
    print(f'Part one: {part_one(input_arr)}')
    print(f'Part two: {part_two(input_arr)}')