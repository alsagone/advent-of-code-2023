import glob
import re
import sys
import platform
import datetime

today = datetime.datetime.now()
annee = today.year

separator = '/' if platform.system() == 'Linux' else '\\'


def get_python_files() -> list[str]:
    return glob.glob("adc*")


def create_file(filename: str) -> None:
    try:
        with open(filename, "x"):
            print(f"{filename} crée")

    except:
        print(f"Erreur lors de la création de {filename}", file=sys.stderr)
        sys.exit(1)

    return


def create_new_file(day: int) -> None:
    script_file = f"adc-{annee}_day-{day:02d}.py"
    input_file = "inputs" + separator + f"input-{day:02d}.txt"

    for f in [script_file, input_file]:
        create_file(f)

    return


def get_file_day(filename: str) -> int:
    pattern = r"adc-\d{4}_day-(\d{2})\.py"
    match = re.match(pattern, filename)

    if (match):
        return int(match.group(1))

    else:
        print(f"Erreur: '{filename}' a un nom incorrect", file=sys.stderr)
        sys.exit(1)


def new_day():
    list_files = get_python_files()
    list_files.sort()
    
    if (len(list_files) > 0):
        last_file = list_files[-1]
        last_day = get_file_day(last_file)

    else:
        last_day = 0

    if (last_day < 25):
        create_new_file(last_day + 1)


if __name__ == "__main__":
    new_day()
