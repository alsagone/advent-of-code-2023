from get_input import get_input
import re

# Part one 
def get_number(line: str) -> int:
    digits = re.findall('[0-9]', line)
    number_str = digits[0] + digits[-1] if digits else ""
    return int(number_str, 10) if number_str else 0

  
def part_one(input_arr: list[str]) -> int:
    total_sum = 0
    
    for line in input_arr:
        total_sum += get_number(line)
    
    return total_sum

# Part 2 

dict_numbers = {
    "one": '1',
    "two": '2',
    "three": '3',
    "four": '4',
    "five": '5',
    "six": '6',
    "seven": '7',
    "eight": '8',
    "nine": '9'
}

pattern_numbers = re.compile(r"(one|two|three|four|five|six|seven|eight|nine|ten|\d)")

def parse_number(number_str: str) -> str:
    global dict_numbers
    return number_str if number_str.isdigit() else dict_numbers[number_str]

def parse_line(line: str) -> int:
    global pattern_numbers
    matches = [parse_number(x) for x in re.findall(pattern_numbers, line)]
    number_str = matches[0] + matches[-1] if matches else ""
    return int(number_str, 10) if number_str else 0


def part_two(input_arr: list[str]) -> int:
    total_sum = 0
    
    for line in input_arr:
        total_sum += parse_line(line)
        
    return total_sum

if __name__ == "__main__":
    input_arr = get_input(1)
    print(f'Part one: {part_one(input_arr)}')
    print(f'Part two: {part_two(input_arr)}')