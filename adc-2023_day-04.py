# Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
from get_input import get_input
import re

pattern_numbers = re.compile(r'(\d+)')
pattern_card_number = re.compile(r'Card *(\d+):')


def parse_card(card_str: str):
    card_number = int(pattern_card_number.match(card_str)[1])

    start_win = card_str.index(":")
    end_win = card_str.index('|')

    win_numbers_str = card_str[start_win:end_win]
    my_numbers_str = card_str[end_win:]

    win_numbers = [int(x) for x in pattern_numbers.findall(win_numbers_str)]
    my_numbers = [int(x) for x in pattern_numbers.findall(my_numbers_str)]
    intersection = list(
        set(win_numbers) & set(my_numbers))

    cards_given = [x for x in range(
        card_number+1, card_number+len(intersection)+1)] if intersection else []

    return {
        "card_number": card_number,
        "win_numbers": win_numbers,
        "my_numbers": my_numbers,
        "intersection": intersection,
        "cards_given": cards_given
    }


def get_card_value(card_str: str) -> int:
    card_value = 0
    card = parse_card(card_str)

    for _ in range(len(card["intersection"])):
        card_value = 2 * card_value if card_value > 0 else 1

    return card_value


def part_one(input_arr: list[str]) -> int:
    total_sum = 0

    for line in input_arr:
        total_sum += get_card_value(line)

    return total_sum


# Part Two
cards_dict = {}
cards_processed = 0

# dict of cards (d[card_number] = card_object) sorted by card_number ascending


def get_dict_cards(cards_arr: list[str]):
    d = {}

    for card_str in cards_arr:
        card = parse_card(card_str)
        d[card["card_number"]] = card

    return dict(sorted(d.items()))


def process_card(card_number):
    global cards_processed
    global cards_dict

    cards_processed += 1
    card = cards_dict[card_number]

    # Calling process card on each card you get
    for card_num in card["cards_given"]:
        process_card(card_num)

    return


def part_two(input_arr: list[str]) -> int:
    global cards_processed
    global cards_dict

    cards_processed = 0
    cards_dict = get_dict_cards(input_arr)

    for card_number in cards_dict.keys():
        process_card(card_number)

    return cards_processed


if __name__ == "__main__":
    input_arr = get_input(4)
    print(f'Part one: {part_one(input_arr)}')
    print(f'Part two: {part_two(input_arr)}')
