"""
Something like: if char is a digit, add char to the ongoing number and check all adjacent symbols for something not ‘.’ if found put the number to true. 
When the next nondigit (or on a new row) is found check if we have a number and if we need to keep it. 
In the end, sum it off. 
"""

from get_input import get_input
import re
import math

grid = []
pattern_symbol = re.compile(r"[^.\d]")


def get_grid(input_arr: list[str]) -> list[list[str]]:
    global grid

    grid = []

    for line in input_arr:
        row = []
        for char in line:
            row.append(char)

        grid.append(row)

    return


def are_valid_coordinates(x: int, y: int):
    return 0 <= x < len(grid) and 0 <= y < len(grid[0])


def get_char_at(x: int, y: int):
    return grid[x][y] if are_valid_coordinates(x, y) else None


def is_a_symbol(x: int, y: int) -> bool:
    char = get_char_at(x, y)
    return char and re.search(pattern_symbol, char)


def is_near_symbol(x: int, y: int):
    b = None

    # Top, Left, Right, Down
    # Diagonal Top Left, Diag. Top Right, Diag Down Left, Diag Down Right
    neighbors_coordinates = [
        [x-1, y],
        [x, y-1],
        [x, y+1],
        [x+1, y],
        [x-1, y-1],
        [x-1, y+1],
        [x+1, y-1],
        [x+1, y+1]
    ]

    for coor in neighbors_coordinates:
        if is_a_symbol(coor[0], coor[1]):
            b = (coor[0], coor[1])
            break

    return b


def part_one(input_arr: list[str]):
    get_grid(input_arr)
    current_number = ""
    symbol_found = False
    total_sum = 0

    for i in range(len(grid)):
        for j in range(len(grid[0])):
            char = get_char_at(i, j)

            if char.isdigit():
                current_number += char
                symbol_found = symbol_found or is_near_symbol(i, j)

            elif symbol_found:
                total_sum += int(current_number, 10)
                current_number = ""
                symbol_found = False

            else:
                current_number = ""

        if symbol_found:
            total_sum += int(current_number, 10)
            symbol_found = False

        current_number = ""

    return total_sum

# Part two


def add_to_dict(key: any, value: any, d: dict[any, any]) -> dict[any, any]:
    if key in d:
        d[key].append(value)

    else:
        d[key] = [value]

    return d


def part_two(input_arr: list[str]):
    get_grid(input_arr)
    current_number = ""
    symbol_found = False
    total_sum = 0

    d = {}
    last_symbol_found = ()

    for i in range(len(grid)):
        for j in range(len(grid[0])):
            char = get_char_at(i, j)

            if char.isdigit():
                current_number += char
                symbol_coordinates = is_near_symbol(i, j)

                if not (symbol_found) and symbol_coordinates:
                    symbol_found = True
                    last_symbol_found = symbol_coordinates

            elif symbol_found:
                n = int(current_number, 10)
                d = add_to_dict(last_symbol_found, n, d)

                current_number = ""
                symbol_found = False

            else:
                current_number = ""
                symbol_found = False

        if symbol_found:
            n = int(current_number, 10)
            d = add_to_dict(last_symbol_found, n, d)

        current_number = ""
        symbol_found = False

    # Answer is the sum of the numbers that have the same symbol near them

    for arr in d.values():
        if len(arr) >= 2:
            total_sum += math.prod(arr)

    return total_sum


if __name__ == "__main__":
    input_arr = get_input(3)
    print(f'Part one: {part_one(input_arr)}')
    print(f'Part two: {part_two(input_arr)}')
