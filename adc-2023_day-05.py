from get_input import get_input
import re
import sys

pattern_numbers = re.compile(r"(\d+)")
pattern_property_name = re.compile(r"([a-z]+)-to-([a-z]+)")

# Helper functions 

def is_between(x: int, bound_1: int, bound_2: int) -> bool:
    if bound_1 < bound_2:
        min_bound = bound_1
        max_bound = bound_2
    else:
        min_bound = bound_2
        max_bound = bound_1
        
    return min_bound <= x <= max_bound

def get_property_names(section_name: str) -> str:
    matches = re.findall(pattern_property_name, section_name)[0]
    if matches:
        return matches
    else:
       return None, None
    
class Seed:
    def __init__(self, seed_number: int):
        self.seed_number = seed_number
        self.soil = None 
        self.fertilizer = None
        self.water = None 
        self.light = None 
        self.temperature = None
        self.humidity = None 
        self.location = None 
        
    def to_dict(self): 
        return {
            'seed_number': self.seed_number,
            'soil': self.soil,
            'fertilizer': self.fertilizer,
            'water': self.soil,
            'light': self.light,
            'temperature': self.temperature,
            'humidity': self.humidity,
            'location': self.location
        }
        
    def set_property(self, property_name: str, value):
        if property_name == 'soil':
            self.soil = value 
            
        elif property_name == 'fertilizer':
            self.fertilizer = value 
            
        elif property_name == 'water':
            self.water = value 
            
        elif property_name == 'light':
            self.light = value 
            
        elif property_name == 'temperature':
            self.temperature = value 
            
        elif property_name == 'humidity':
            self.humidity = value
            
        elif property_name == 'location':
            self.location = value
            
        else:
            print(f'Unknown property name: {property_name}', file=sys.stderr)
            exit(1)
            
        return 
        
    def get_property(self, property_name: str) -> int:
        if property_name == "seed":
            value = self.seed_number
            
        elif property_name == 'soil':
            value = self.soil
            
        elif property_name == 'fertilizer':
            value = self.fertilizer 
            
        elif property_name == 'water':
            value = self.water
            
        elif property_name == 'light':
            value = self.light
            
        elif property_name == 'temperature':
            value = self.temperature 
            
        elif property_name == 'humidity':
            value = self.humidity
            
        elif property_name == 'location':
            value = self.location
            
        else:
            print(f'Unknown property name: {property_name}', file=sys.stderr)
            exit(1)
            
        return value
   

seed_list: list[Seed] = []
rules = {}

def get_rules(input_arr):
    global rules
    rules = {}
    
    for section_name in get_sections(input_arr):        
        data = get_data(input_arr, section_name)
        from_property, to_property = get_property_names(section_name)
        rule_name = f'{from_property}-to-{to_property}'
        rules[rule_name] = []
        
        for line in data:
            dest_start, source_start, range_length  = [int(x) for x in re.findall(pattern_numbers, line)]
            r = {
                'source_from': source_start,
                'source_to': source_start+range_length-1,
                'dest_from': dest_start,
                'dest_to': dest_start+range_length-1
            }
            rules[rule_name].append(r)
            
    return rules 
            
               
def parse_rule(rule_name: str):
    global seed_list
    global rules 
    
    from_property, to_property = get_property_names(rule_name)
    
    seed_map = {}
        
    for seed in seed_list:
        seed_from_property_value = seed.get_property(from_property)
        
        for r in rules[rule_name]:
            if is_between(seed_from_property_value, r['source_from'], r['source_to']):
                offset = seed_from_property_value - r['source_from']
                seed_destination = r['dest_from'] + offset
                seed_map[seed_from_property_value] = seed_destination
                
    for seed in seed_list:
        seed_from_property_value = seed.get_property(from_property)
        value = seed_map.get(seed_from_property_value)
        seed.set_property(to_property, value if value else seed_from_property_value)
        
                
    return 


def get_data(input_arr: list[str], rule_name: str):
    section_start = input_arr.index(rule_name)
    
    if rule_name == 'humidity-to-location map:':
        section_end = len(input_arr) 
        
    else:
        section_end = input_arr.index('', section_start) 

    #  the destination range start, the source range start, and the range length.

    return input_arr[section_start+1:section_end]

def get_sections(input_arr: list[str]):
    return [line for line in input_arr if pattern_property_name.match(line)]

def place_seeds():
    for rule_name in rules.keys():
        parse_rule(rule_name)
    return 

def get_min_location() -> int:   
    return min([s.get_property("location") for s in seed_list])

def init_part_one(seed_line: str):
    global seed_list
    seed_list = []
    
    arr = [int(x) for x in re.findall(pattern_numbers, seed_line)]
    
    for n in arr:
        seed_list.append(Seed(n))
        
def part_one(input_arr: list[str]):
    init_part_one(input_arr[0])
    place_seeds()
    return get_min_location()
    

# Part Two 
def init_part_two(seed_line: str):
    global seed_list
    seed_list = []
    arr = [int(x) for x in re.findall(pattern_numbers, seed_line)]
    
    for i in range(0, len(arr), 2):
        seed_number = arr[i]
        range_length = arr[i+1]
        max_seed = seed_number + range_length - 1
        
        
        seed_list.append(Seed(seed_number))
        seed_list.append(Seed(max_seed))
            
    return 

def part_two(input_arr: list[str]):
    init_part_two(input_arr[0])
    place_seeds()
    return get_min_location()

if __name__ == "__main__":
    input_arr = get_input(0)    
    get_rules(input_arr)
    print(rules.keys())
    print(f'Part one: {part_one(input_arr)}')
    print(f'Part two: {part_two(input_arr)}')
